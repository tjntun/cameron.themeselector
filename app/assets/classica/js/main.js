jQuery(document).ready(function($) {
    // Theme picker
    $('#editTools .picker .group').click(function() {
        $('#editTools .group').removeClass('selected');
        $(this).addClass('selected');

        var themeColor = $(this).attr('theme-color');
        var themeBg = $(this).attr('theme-bg');
        $('body').removeClass('theme-color-0 theme-color-1 theme-color-2 theme-color-3 theme-color-4').addClass('theme-color-'+themeColor);
        $('body').removeClass('theme-bg-1 theme-bg-2').addClass('theme-bg-'+themeBg);
        initMap(themeColor);
    });

    // Load default font
    var fontBig = localStorage.getItem('cameron_font_big');
    if(fontBig != null) {
        $('.font-big').css('font-family', fontBig);

        $('#font-big-select [value='+fontBig+']').attr('selected',true);
    }
    var fontSmall = localStorage.getItem('cameron_font_small');
    if(fontSmall != null) {
        $('.font-small').css('font-family', fontSmall);
        $('#font-small-select [value='+fontSmall+']').attr('selected',true);
    }

    // Font picker
    $('#font-big-select').change(function(event) {
        var font = $(this).val();
        $('.font-big').css('font-family', font);
        localStorage.setItem('cameron_font_big', font);
    });
    $('#font-small-select').change(function(event) {
        var font = $(this).val();
        $('.font-small, .content ul, p').css('font-family', font);
        localStorage.setItem('cameron_font_small', font);
    });

    // Nicescroll
    $("html").niceScroll();

    // Parallax
    $('.parallax').parallax({});

    // Home section height
    if(isMobile.any) {
        $('#home').height($(window).height());
    }

    // Mobile menu
    $('#humburger').click(function(event) {
        event.stopPropagation();
        $('#mobile-nav').slideDown('fast');
    });
    if($(window).width() < 1200){
        $('#mobile-nav .close').click(function() {
            $('#mobile-nav').slideUp('fast');
        });
        $('#mobile-nav').click(function(event) {
            event.stopPropagation();
        });
    }

    // Sticky nav
    $(window).on('scroll', function(event) {
        stickyNav();
        updateActiveNav();
    });
    function stickyNav() {
        if($(window).width() > 1200){
            var scrollTop = $(window).scrollTop();
            if (scrollTop >= $('#features').offset().top) {
                $('#secondary-nav').addClass('sticky');
            } else {
                $('#secondary-nav').removeClass('sticky');
            }
        }
    }

    updateActiveNav();
    
    // Active nav
    function updateActiveNav() {
        var scrollTop = $(window).scrollTop();
        $('#wrapper > section').each(function(index, el) {
            var id = $(this).attr('id');
            var adjust = 10;
            if(id == 'contact') adjust = 100;
            if (scrollTop >= ($(this).offset().top - adjust) && scrollTop <= ($(this).offset().top + $(this).outerHeight())) {
                $('nav a').removeClass('active');
                $('nav a[href=#'+id+']').not('.logo').addClass('active');
            }
        });
    }

    // Smooth scroll
    $('a[href*="#"]:not([href="#"])').click(function() {
        stickyNav();
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });

    // Royal slider
    var si = $('#gallery-1').royalSlider({
        addActiveClass: true,
        arrowsNav: false,
        controlNavigation: 'none',
        autoScaleSlider: true,
        autoScaleSliderWidth: 960,
        autoScaleSliderHeight: 340,
        loop: true,
        fadeinLoadedSlide: false,
        globalCaption: true,
        keyboardNavEnabled: true,
        globalCaptionInside: false,

        visibleNearby: {
            enabled: true,
            centerArea: 0.5,
            center: true,
            breakpoint: 650,
            breakpointCenterArea: 0.64,
            navigateByCenterClick: true
        }
    }).data('royalSlider');

    $('#gallery-nav .current').text(si.currSlideId + 1);
    $('#gallery-nav .total').text(si.numSlides);

    $('#gallery-nav .next').click(function(event) {
        si.next();
        $('#gallery-nav .current').text(si.currSlideId + 1);
    });
    $('#gallery-nav .prev').click(function(event) {
        si.prev();
        $('#gallery-nav .current').text(si.currSlideId + 1);
    });
    si.ev.on('rsAfterSlideChange', function(event) {
        $('#gallery-nav .current').text(si.currSlideId + 1);
    });

    // Location
    $('.mapwrapper .icon').click(function(event) {
        $('.mapwrapper.full').toggle();
        initMap(null, 'map2');
    });

    // Map
    // Google map
    var map;

    function initMap(themeNumber, mapId) {
        console.log(mapId)
        if(themeNumber == undefined || themeNumber == null) themeNumber = 0;
        if(mapId == undefined || mapId == null) mapId = "map";
        var place = { lat: 54.515102, lng: -128.610764 };
        map = new google.maps.Map(document.getElementById(mapId), {
            center: place,
            zoom: 13
        });
        var icon = 'img/icon/theme-color-'+ themeNumber +'/marker.png?t='+Date.now();
        var marker = new google.maps.Marker({
            position: place,
            map: map,
            icon: icon
        });
    }
    initMap();
});
