;(function() {
"use strict";

angular.module('upsites', [
    'angular.css.injector',
    'smoothScroll',
    'ui.sortable'
]).config(["$sceDelegateProvider", function($sceDelegateProvider) {
    $sceDelegateProvider.resourceUrlWhitelist([
        // Allow same origin resource loads.
        'self',
        // Allow loading from our assets domain.  Notice the difference between * and **.
        'https://www.youtube.com/**'
    ]);
}]);
}());

;(function() {
"use strict";

EditToolsCtrl.$inject = ["$scope", "$rootScope", "$timeout", "cssInjector"];
angular.module('upsites')
    .controller('EditToolsCtrl', EditToolsCtrl);

function EditToolsCtrl($scope, $rootScope, $timeout, cssInjector) {
    var editToolsVm = this;

    editToolsVm.openThemeSelector = function() {
        angular.element('#themeSelector').removeClass('hided');
    }
    var nicescrollInited = false;
    editToolsVm.openThemeReorder = function() {
        if (!nicescrollInited) {
            _j('#themeReorder').niceScroll();
            nicescrollInited = true;
        }
        angular.element('#themeReorder').removeClass('hided');
    }

    $timeout(function() {
        // Theme picker
        var themeColor = localStorage.getItem('cameron_theme_color');
        if (themeColor == null) { themeColor = 0; }
        var themeBg = localStorage.getItem('cameron_theme_bg');
        if (themeBg == null) { themeBg = 1; }
        changeTheme(themeColor, themeBg);

        _j('#editTools .picker .group').click(function() {
            var themeColor = _j(this).attr('theme-color');
            var themeBg = _j(this).attr('theme-bg');
            changeTheme(themeColor, themeBg);

            localStorage.setItem('cameron_theme_color', themeColor);
            localStorage.setItem('cameron_theme_bg', themeBg);
        });

        function changeTheme(themeColor, themeBg) {
            $rootScope.$emit('changeTheme', themeColor);
            // Mark color pallete as selected
            _j('#editTools .group').removeClass('selected');
            _j('#editTools .group[theme-color="' + themeColor + '"][theme-bg="' + themeBg + '"]').addClass('selected');

            _j('body').removeClass('theme-color-0 theme-color-1 theme-color-2 theme-color-3 theme-color-4').addClass('theme-color-' + themeColor);
            _j('body').removeClass('theme-bg-1 theme-bg-2').addClass('theme-bg-' + themeBg);

            $rootScope.$emit('evt.themeColorChanged', { themeColor: themeColor, themeBg: themeBg });
        }

        // Load default font
        var fontBig = localStorage.getItem('cameron_font_big');
        if (fontBig != null) {
            _j('.font-big').css('font-family', fontBig);
            _j('#font-big-select [value=' + fontBig + ']').attr('selected', true);
        } else {
            fontBig = _j('#font-big-select').val();
            _j('.font-big').css('font-family', fontBig);
        }

        var fontSmall = localStorage.getItem('cameron_font_small');
        if (fontSmall != null) {
            _j('.font-small').css('font-family', fontSmall);
            _j('#font-small-select [value=' + fontSmall + ']').attr('selected', true);
        } else {
            fontSmall = _j('#font-small-select').val();
            _j('.font-small').css('font-family', fontSmall);
        }

        // Font picker
        _j('#font-big-select').change(function(event) {
            var font = _j(this).val();
            _j('.font-big').css('font-family', font);
            localStorage.setItem('cameron_font_big', font);
        });
        _j('#font-small-select').change(function(event) {
            var font = _j(this).val();
            _j('.font-small, .content ul, p').css('font-family', font);
            localStorage.setItem('cameron_font_small', font);
        });
    });
}
}());

;(function() {
"use strict";

MainCtrl.$inject = ["$scope", "$rootScope", "$timeout", "cssInjector"];
angular.module('upsites')
    .controller('MainCtrl', MainCtrl);

function MainCtrl($scope, $rootScope, $timeout, cssInjector) {
    $rootScope.themeChangeableSections = [{
        id: 'features',
        title: 'Details',
    }, {
        id: 'gallery',
        title: 'Gallery',
    }, {
        id: 'location',
        title: 'Location',
    }, {
        id: 'auction',
        title: 'Inspections & Selling',
    }, {
        id: 'agent',
        title: 'Selling Agent',
    }, ];
    var mainVm = this;
    mainVm.themes = ['classica', 'nuevo', 'stream'];

    mainVm.currentTheme = mainVm.themes[0];
    applyTheme(mainVm.currentTheme);

    $scope.$watch('mainVm.currentTheme', function(theme, oldTheme) {
        if (theme != oldTheme) {
            applyTheme(theme);
            $timeout(function() {
                $rootScope.$emit('evt.toggleThemeSelector', 'hide');
            }, 1000);
        }
    });

    function applyTheme(theme) {
        cssInjector.removeAll();
        cssInjector.add("assets/" + theme + "/css/main.css");
        cssInjector.add("assets/" + theme + "/css/responsive.css");
        cssInjector.add("assets/" + theme + "/css/theme.css");
        cssInjector.add("assets/" + theme + "/fonts/web/fonts.css");

        if (theme == 'classica') {
            cssInjector.add("assets/" + theme + "/fonts/classica/style.css");
        }
        if (theme == 'nuevo') {
            cssInjector.add("assets/" + theme + "/fonts/iconfonts/icon-fonts.css");
        }
        if (theme == 'stream') {
            cssInjector.add("assets/" + theme + "/fonts/icomoon/icons.css");
            cssInjector.add("assets/" + theme + "/vendor/mmenu/css/jquery.mmenu.all.css");
        }
    }
}
}());

;(function() {
"use strict";

ThemeClassicaCtrl.$inject = ["$scope", "$rootScope", "$timeout", "$window", "$sce"];
angular.module('upsites')
    .controller('ThemeClassicaCtrl', ThemeClassicaCtrl);

function ThemeClassicaCtrl($scope, $rootScope, $timeout, $window, $sce) {
    var vm = this;
    vm.featuresReadmore = false;
    vm.agents = [{
        id: 1,
        avatar: 'assets/classica/img/samantha.jpg',
        firstname: 'SAMANTHA',
        lastname: 'PAULINSON',
        quote: 'It makes me proud to be part of a team with such high integrity and commitment to serving our clients',
        about: 'A trusted advisor with an excellent sales record, John O’Reilly is an influential member to the McGrath team in the Thirroul Office. Specialising in the suburbs of Woonona, Bulli, Thirroul and Austinmer, Johns’ passion for his work is almost equal to that for his two daughters. He brings a high energy approach to his work and has unparalleled drive to continually raise the benchmark. While outcome driven, Johns’ caring attitude stems from his motivation to match the right buyers with their dream home. A commitment to ongoing industry training and professional development sets John apart from the average real estate agent, making him a perfect fit with the McGrath network. Johns’ professional ambition is closely aligned with McGrath’s ethos of continuously improving staff skills to ensure a ‘six star’ quality of service. For residential sales in Thirroul and surrounding areas, contact John O’Reilly to find out how his skills and experience can benefit your sale.',
        video: 'https://www.youtube.com/embed/Jer8XjMrUB4'
    }, {
        id: 2,
        avatar: 'assets/classica/img/robert.jpg',
        firstname: 'ROBERT',
        lastname: 'MICHAELS',
        quote: 'It makes me proud to be part of a team',
        about: 'ROBERT passion for his work is almost equal to that for his two daughters. He brings a high energy approach to his work and has unparalleled drive to continually raise the benchmark. While outcome driven, Johns’ caring attitude stems from his motivation to match the right buyers with their dream home. A commitment to ongoing industry training and professional development sets John apart from the average real estate agent, making him a perfect fit with the McGrath network. Johns’ professional ambition is closely aligned with McGrath’s ethos of continuously improving staff skills to ensure a ‘six star’ quality of service. For residential sales in Thirroul and surrounding areas, contact John O’Reilly to find out how his skills and experience can benefit your sale.',
        video: 'https://www.youtube.com/embed/4KSlB-eA3u4'
    }];
    vm.selectedAgent = vm.agents[0];
    $sce.trustAsResourceUrl(vm.selectedAgent.video);

    vm.closeMenu = closeMenu;
    vm.openMenu = openMenu;
    vm.openSidePanel = openSidePanel;
    vm.closeSidePanel = closeSidePanel;

    ///////////////

    function openSidePanel(agent) {
        vm.selectedAgent = agent;
        $sce.trustAsResourceUrl(vm.selectedAgent.video);
        vm.slideIn = true;
        _j(".niceScroll").niceScroll();
    }

    function closeSidePanel() {
        vm.slideIn = false;
        _j(".niceScroll").getNiceScroll().remove();
    }

    function openMenu() {
        _j('#mobile-nav').slideDown('fast');
    }

    function closeMenu() {
        _j('#mobile-nav').slideUp('fast');
    }

    // Show/hide video iframe
    _j('.video-modal').on('show.bs.modal', function() {
        var src = _j(this).find('iframe').data('src');
        _j(this).find('iframe').attr('src', src);
    });
    _j('.video-modal').on('hide.bs.modal', function() {
        _j(this).find('iframe').attr('src', '');
    });

    // Destroy Stream theme mobile nav
    _j('body > #mobile-nav').remove();
    _j('body > .parallax-mirror').remove();

    // Map
    var map;

    function initMap(themeNumber, mapId) {
        if (themeNumber == undefined || themeNumber == null) themeNumber = 0;
        if (mapId == undefined || mapId == null) mapId = "map";
        var place = { lat: 54.515102, lng: -128.610764 };
        map = new google.maps.Map(document.getElementById(mapId), {
            center: place,
            zoom: 13
        });
        google.maps.event.trigger(map, 'resize');
        var icon = 'assets/classica/img/icon/theme-color-' + themeNumber + '/marker.png?t=' + Date.now();
        var marker = new google.maps.Marker({
            position: place,
            map: map,
            icon: icon
        });
        $timeout(function() {
            google.maps.event.trigger(map, 'resize');
            map.setCenter(place);
        }, 1000);
    }

    // Listen for theme change event
    $rootScope.$on('changeTheme', function(event, themeColor) {
        initMap(themeColor);
    });

    $timeout(function function_name() {
        // _j(".niceScroll").niceScroll();

        $rootScope.$on('evt.orderChanged', function() {
            smoothScroll();
            updateActiveNav();
        });

        if (isMobile.any) {
            angular.element('#home').height(angular.element(window).height());
        }
        var themeColor = localStorage.getItem('cameron_theme_color');
        if (themeColor) {
            initMap(themeColor);
        } else {
            initMap(0);
        }

        smoothScroll();

        function smoothScroll() {
            // Smooth scroll
            _j('a[href*="#"]:not([href="#"])').click(function() {
                stickyNav();
                if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                    var target = _j(this.hash);
                    target = target.length ? target : _j('[name=' + this.hash.slice(1) + ']');
                    if (target.length) {
                        _j('html, body').animate({
                            scrollTop: target.offset().top
                        }, 1000);
                        return false;
                    }
                }
            });
        }

        // Sticky nav
        angular.element($window).bind('scroll', function(event) {
            stickyNav();
            updateActiveNav();
        });

        function stickyNav() {
            // if ($window.innerWidth > 1200) {
            var scrollTop = $window.pageYOffset;
            if (scrollTop >= _j('#home').height()) {
                angular.element('#secondary-nav').addClass('sticky');
            } else {
                angular.element('#secondary-nav').removeClass('sticky');
            }
            // }
        }

        updateActiveNav();

        // Active nav
        function updateActiveNav() {
            var scrollTop = $window.pageYOffset;
            _j('#wrapper section').each(function(index, el) {
                var id = _j(this).attr('id');
                var adjust = 10;
                if (id == 'contact') adjust = 100;
                if (scrollTop >= (_j(this).offset().top - adjust) && scrollTop <= (_j(this).offset().top + _j(this).outerHeight())) {
                    _j('nav a').removeClass('active');
                    _j('nav a[href="#' + id + '"]').not('.logo').addClass('active');
                }
            });
        }
        // Mobile menu
        // _j('#humburger').click(function(event) {
        //     event.stopPropagation();
        //     _j('#mobile-nav').slideDown('fast');
        // });
        if (_j(window).width() < 1200) {
            // _j('#mobile-nav .close').click(function() {
            //     _j('#mobile-nav').slideUp('fast');
            // });
            _j('#mobile-nav').click(function(event) {
                event.stopPropagation();
            });
        }

        // Royal slider
        var si = _j('#gallery-1').royalSlider({
            addActiveClass: true,
            arrowsNav: false,
            controlNavigation: 'none',
            loop: true,
            fadeinLoadedSlide: false,
            globalCaption: true,
            navigateByClick: false,
            keyboardNavEnabled: true,
            globalCaptionInside: false,
            video: {
                autoHideArrows: true,
                autoHideControlNav: false,
                autoHideBlocks: true,
                youTubeCode: '<iframe src="https://www.youtube.com/embed/%id%?rel=1&autoplay=1&showinfo=0" frameborder="no" allowFullscreen></iframe>'
            },
        }).data('royalSlider');

        _j('#gallery-nav .current').text(si.currSlideId + 1);
        _j('#gallery-nav .total').text(si.numSlides);

        _j('#gallery-nav .next').click(function(event) {
            si.next();
            _j('#gallery-nav .current').text(si.currSlideId + 1);
        });
        _j('#gallery-nav .prev').click(function(event) {
            si.prev();
            _j('#gallery-nav .current').text(si.currSlideId + 1);
        });
        si.ev.on('rsAfterSlideChange', function(event) {
            _j('#gallery-nav .current').text(si.currSlideId + 1);
            if (si.currSlideId != 0) {
                _j('#gallery .section-title, #gallery .hr').addClass('fade');
            } else {
                _j('#gallery .section-title, #gallery .hr').removeClass('fade');
            }
        });

        // Location
        _j('#mapModal').on('shown.bs.modal', function() {
            initMap(null, 'map2');
        });

        _j('#lifestyle .launch').magnificPopup({
            items: [{
                src: 'assets/classica/img/gallery-preview.jpg'
            }, {
                src: 'https://vimeo.com/185383679',
                type: 'iframe' // this overrides default type
            }],
            gallery: {
                enabled: true
            },
            type: 'image' // this is default type
        });
    }, 800);
}
}());

;(function() {
"use strict";

ThemeNuevoCtrl.$inject = ["$scope", "$rootScope", "$timeout"];
angular.module('upsites')
    .controller('ThemeNuevoCtrl', ThemeNuevoCtrl);

function ThemeNuevoCtrl($scope, $rootScope, $timeout) {
    var vm = this;
    // Destroy Stream theme mobile nav
    _j('body > #mobile-nav').remove();
    
    // $rootScope.$on('evt.themeColorChanged', function(e, data) {
    //     console.log(data);
    //     initMap(data.themeColor);
    // });

    // Google map
    var map;

    function initMap(themeNumber, mapId) {
        if (themeNumber == undefined || themeNumber == null) themeNumber = 0;
        if (mapId == undefined || mapId == null) mapId = "map";
        var place = { lat: 54.515102, lng: -128.610764 };
        map = new google.maps.Map(document.getElementById(mapId), {
            center: place,
            zoom: 13,
            styles: [{ "featureType": "water", "elementType": "geometry", "stylers": [{ "color": "#e9e9e9" }, { "lightness": 17 }] }, { "featureType": "landscape", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 20 }] }, { "featureType": "road.highway", "elementType": "geometry.fill", "stylers": [{ "color": "#ffffff" }, { "lightness": 17 }] }, { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{ "color": "#ffffff" }, { "lightness": 29 }, { "weight": 0.2 }] }, { "featureType": "road.arterial", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 18 }] }, { "featureType": "road.local", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 16 }] }, { "featureType": "poi", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 21 }] }, { "featureType": "poi.park", "elementType": "geometry", "stylers": [{ "color": "#dedede" }, { "lightness": 21 }] }, { "elementType": "labels.text.stroke", "stylers": [{ "visibility": "on" }, { "color": "#ffffff" }, { "lightness": 16 }] }, { "elementType": "labels.text.fill", "stylers": [{ "saturation": 36 }, { "color": "#333333" }, { "lightness": 40 }] }, { "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "transit", "elementType": "geometry", "stylers": [{ "color": "#f2f2f2" }, { "lightness": 19 }] }, { "featureType": "administrative", "elementType": "geometry.fill", "stylers": [{ "color": "#fefefe" }, { "lightness": 20 }] }, { "featureType": "administrative", "elementType": "geometry.stroke", "stylers": [{ "color": "#fefefe" }, { "lightness": 17 }, { "weight": 1.2 }] }]
        });
        var icon = 'assets/nuevo/img/icon/marker' + themeNumber + '.svg?t=' + Date.now();
        var marker = new google.maps.Marker({
            position: place,
            map: map,
            icon: icon
        });

        $timeout(function() {
            google.maps.event.trigger(map, 'resize');
            map.setCenter(place);
        }, 1000);
    }

    $timeout(function() {
        // Home section height
        if (isMobile.any) {
            angular.element('#home').css('height', _j(window).height() + 'px');
        }
        // All section height
        if (!isMobile.any) {
            angular.element('theme-section section').css('height', _j(window).height() + 'px');
            _j(window).resize(function(event) {
                _j('#wrapper > section').not('#contact').height(_j(window).height());
            });
        }
        
        // Nicescroll
        _j("html").niceScroll();
        if (!isMobile.any) _j(".niceScroll").niceScroll();

        // Mobile menu
        _j('#humburger').click(function(event) {
            event.stopPropagation();
            _j('#mobile-nav').slideDown('fast');
        });
        if (_j(window).width() < 1200) {
            _j('#mobile-nav .close').click(function() {
                _j('#mobile-nav').slideUp('fast');
            });
            _j('#mobile-nav').click(function(event) {
                event.stopPropagation();
            });
        }

        // Sticky nav
        _j(window).on('scroll', function(event) {
            stickyNav();
            updateActiveNav();
        });
        stickyNav();

        function stickyNav() {
            // if (_j(window).width() > 1200) {
            var scrollTop = _j(window).scrollTop();
            if (scrollTop >= _j('#features').offset().top) {
                _j('#secondary-nav').addClass('sticky');
            } else {
                _j('#secondary-nav').removeClass('sticky');
            }
            // }
        }

        updateActiveNav();



        // Feature section auto scroll
        if (_j(window).height() <= 900) {
            _j('#features .content').removeClass('flex-center-y').addClass('scrollable');
        }

        // Active nav
        var currentPageId = 'home';

        function updateActiveNav() {
            var scrollTop = _j(window).scrollTop();
            _j('#wrapper section').each(function(index, el) {
                var id = _j(this).attr('id');
                var adjust = 10;
                // Change current page title on #secondary-nav (aka sticky nav)
                if (scrollTop >= (_j(this).offset().top - adjust) && scrollTop < (_j(this).offset().top + _j(this).outerHeight())) {
                    var currentPageTitle = _j('#' + id + ' .section-title').text();
                    currentPageId = id;
                    if (id == 'gallery') currentPageTitle = 'Gallery';
                    if (id == 'auction') currentPageTitle = 'Inspections';
                    if (currentPageTitle != _j('#currentPageTitle .inner').text()) {
                        _j("#currentPageTitle .inner").fadeOut(function() {
                            _j(this).text(currentPageTitle).fadeIn();
                        });
                    }

                }
            });
        }

        // Next/prev section
        _j('#secondary-nav .icon-arrowdown').click(function() {
            // next
            var nextPageId = _j('#' + currentPageId).next().attr('id');
            scrollTo(_j('#' + nextPageId));
        });
        _j('#secondary-nav .icon-arrowup').click(function() {
            // back
            var prevPageId = _j('#' + currentPageId).prev().attr('id');
            scrollTo(_j('#' + prevPageId));
        });

        // Smooth scroll
        _j('a[href*="#"]:not([href="#"])').click(function() {
            stickyNav();
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = _j(this.hash);
                return scrollTo(target);
            }
        });

        function scrollTo(target) {
            target = target.length ? target : _j('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                _j('html, body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
            return false;
        }

        // Square gallery
        try {
            new SquareGallery({
                container: 'gallery',
                // qtyColumns: 3
            });
        } catch (e) {
            console.log(e)
        }
        initMap(0);
    }, 200);

}
}());

;(function() {
"use strict";

ThemeReorderCtrl.$inject = ["$rootScope", "$scope"];
angular.module('upsites')
    .controller('ThemeReorderCtrl', ThemeReorderCtrl);

function ThemeReorderCtrl($rootScope, $scope) {
    var themeReorderVm = this,
        nicescrollInited = false;
    themeReorderVm.themeChangeableSections = $rootScope.themeChangeableSections;

    themeReorderVm.sortableOptions = {
        appendTo: '#reorderSections',
        update: function () {
            $rootScope.$emit('evt.orderChanged');
        }
    }

    ////////

    themeReorderVm.hideThemeReorder = function() {
        angular.element('#themeReorder').addClass('hided');
    }

    themeReorderVm.showThemeReorder = function() {
        
        angular.element('#themeReorder').removeClass('hided');
    }

    $rootScope.$on('evt.toggleThemeReorder', function(event, status) {
        if (status == 'show') {
            themeReorderVm.showThemeReorder();
        } else {
            themeReorderVm.hideThemeReorder();
        }
    });
}
}());

;(function() {
"use strict";

ThemeSelectorCtrl.$inject = ["$rootScope"];
angular.module('upsites')
    .controller('ThemeSelectorCtrl', ThemeSelectorCtrl);

function ThemeSelectorCtrl($rootScope) {
    var themeSelectorVm = this;

    themeSelectorVm.hideThemeSelector = function() {
        angular.element('#themeSelector').addClass('hided');
    }

    themeSelectorVm.showThemeSelector = function() {
        angular.element('#themeSelector').removeClass('hided');
    }

    $rootScope.$on('evt.toggleThemeSelector', function(event, status) {
        if (status == 'show') {
            themeSelectorVm.showThemeSelector();
        } else {
            themeSelectorVm.hideThemeSelector();
        }
    });
}
}());

;(function() {
"use strict";

ThemeStreamCtrl.$inject = ["$timeout", "$rootScope", "$scope"];
angular.module('upsites')
    .controller('ThemeStreamCtrl', ThemeStreamCtrl);

function ThemeStreamCtrl($timeout, $rootScope, $scope) {
    var vm = this;

    vm.nextSlide = function(slideId) {
        if (!slideId) slideId = 'carousel';
        _j('#' + slideId).carousel('next');
    }
    vm.prevSlide = function(slideId) {
        if (!slideId) slideId = 'carousel';
        _j('#' + slideId).carousel('prev');
    }

    vm.toggleMobileMenu = function() {
        _j('#mobile-nav').slideToggle('fast', 'swing');
    }

    // Google map
    var map;

    function initMap(themeNumber, mapId) {
        try {
            if (themeNumber == undefined || themeNumber == null) themeNumber = 0;
            if (mapId == undefined || mapId == null) mapId = "map";
            var place = { lat: 54.515102, lng: -128.610764 };
            map = new google.maps.Map(document.getElementById(mapId), {
                center: place,
                zoom: 13,
                scrollwheel: false,
                styles: [{ "featureType": "water", "elementType": "geometry", "stylers": [{ "color": "#e9e9e9" }, { "lightness": 17 }] }, { "featureType": "landscape", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 20 }] }, { "featureType": "road.highway", "elementType": "geometry.fill", "stylers": [{ "color": "#ffffff" }, { "lightness": 17 }] }, { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{ "color": "#ffffff" }, { "lightness": 29 }, { "weight": 0.2 }] }, { "featureType": "road.arterial", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 18 }] }, { "featureType": "road.local", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 16 }] }, { "featureType": "poi", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 21 }] }, { "featureType": "poi.park", "elementType": "geometry", "stylers": [{ "color": "#dedede" }, { "lightness": 21 }] }, { "elementType": "labels.text.stroke", "stylers": [{ "visibility": "on" }, { "color": "#ffffff" }, { "lightness": 16 }] }, { "elementType": "labels.text.fill", "stylers": [{ "saturation": 36 }, { "color": "#333333" }, { "lightness": 40 }] }, { "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "transit", "elementType": "geometry", "stylers": [{ "color": "#f2f2f2" }, { "lightness": 19 }] }, { "featureType": "administrative", "elementType": "geometry.fill", "stylers": [{ "color": "#fefefe" }, { "lightness": 20 }] }, { "featureType": "administrative", "elementType": "geometry.stroke", "stylers": [{ "color": "#fefefe" }, { "lightness": 17 }, { "weight": 1.2 }] }]
            });
            var icon = 'assets/stream/img/icon/marker' + themeNumber + '.svg?t=' + Date.now();
            var marker = new google.maps.Marker({
                position: place,
                map: map,
                icon: icon
            });

            $timeout(function() {
                google.maps.event.trigger(map, 'resize');
                map.setCenter(place);
            }, 1000);
        } catch (e) {
            console.log(e);
        }
    }

    $timeout(function() {
        var gallerySlider = _j('#carousel .royalSlider').data('royalSlider');

        vm.gallerySliderNext = function() {
            gallerySlider.next();
        }
        vm.gallerySliderPrev = function() {
            gallerySlider.prev();
        }
        var videoSlider = _j('#video-carousel .royalSlider').data('royalSlider');

        vm.videoSliderNext = function() {
            videoSlider.next();
        }
        vm.videoSliderPrev = function() {
            videoSlider.prev();
        }

        // Show/hide video iframe
        _j('.video-modal').on('show.bs.modal', function() {
            var src = _j(this).find('iframe').data('src');
            _j(this).find('iframe').attr('src', src);
        });
        _j('.video-modal').on('hide.bs.modal', function() {
            _j(this).find('iframe').attr('src', '');
        });

        initMap(0);
        // Nicescroll

        if (!isMobile.any) {
            _j("html").niceScroll();
        }

        // Home section height
        if (isMobile.any) {
            _j('#home').height(window.innerHeight);
            angular.element('.nicescroll-rails').remove();
        }

        // Re-position features item
        if (_j(window).width() < 568) {
            vm.featureMustReArrange = true;
        } else {
            vm.featureMustReArrange = false;
        }
        _j(window).on('resize', function(event) {
            if (_j(window).width() < 568) {
                vm.featureMustReArrange = true;
            } else {
                vm.featureMustReArrange = false;
            }
        });

        // Sticky nav
        _j(window).on('scroll', function(event) {
            stickyNav();
            updateActiveNav();
        });
        stickyNav();

        function stickyNav() {
            try {
                var scrollTop = _j(window).scrollTop();
                if (scrollTop > 0 && scrollTop >= _j('#home').height()) {
                    _j('#secondary-nav').addClass('sticky');
                } else {
                    _j('#secondary-nav').removeClass('sticky');
                }
            } catch (e) {
                console.log(e);
            }
        }

        updateActiveNav();

        // All section height

        if (!isMobile.any) {
            _j('#wrapper > section').not('#contact').css('min-height', _j(window).height() + 'px');
            _j(window).resize(function(event) {
                _j('#wrapper > section').not('#contact').css('min-height', _j(window).height() + 'px');
            });
        } else {
            _j('#gallery').css('min-height', _j(window).height() + 'px');
        }

        // Active nav
        var currentPageId = 'home';

        function updateActiveNav() {
            var scrollTop = _j(window).scrollTop();
            _j('#wrapper section').each(function(index, el) {
                var id = _j(this).attr('id');
                var adjust = 85;
                if (scrollTop >= (_j(this).offset().top - adjust) && scrollTop <= (_j(this).offset().top + _j(this).outerHeight())) {
                    _j('nav a').removeClass('active');
                    _j('nav a[href="#' + id + '"]').not('.logo').addClass('active');
                }
            });
        }

        // Next/prev section
        _j('#secondary-nav .icon-arrowdown').click(function() {
            // next
            var nextPageId = _j('#' + currentPageId).next().attr('id');
            scrollTo(_j('#' + nextPageId));
        });
        _j('#secondary-nav .icon-arrowup').click(function() {
            // back
            var prevPageId = _j('#' + currentPageId).prev().attr('id');
            scrollTo(_j('#' + prevPageId));
        });

        // Smooth scroll
        _j('a[href*="#"]:not([href="#"])').click(function() {
            stickyNav();
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = _j(this.hash);
                if (_j(this).parents('nav').attr('id') == 'mobile-nav') vm.toggleMobileMenu();
                return scrollTo(target);
            }
        });

        function scrollTo(target) {
            target = target.length ? target : _j('[name=' + this.hash.slice(1) + ']');
            var adjust = 80;
            if (target.attr('id') == 'features') adjust = 0;
            if (target.length) {
                _j('html, body').animate({
                    scrollTop: target.offset().top - adjust
                }, 1000);
                return false;
            }
            return false;
        }

    }, 500);


}
}());

;(function() {
"use strict";

angular.module('upsites')
    .directive('editTools', ["$compile", function($compile) {
        return {
            restrict: 'E',
            templateUrl: 'views/editTools.html',
            controller: 'EditToolsCtrl',
            controllerAs: 'editToolsVm'
        };
    }]);
}());

;(function() {
"use strict";

angular.module('upsites')
    .directive('themeClassica', function() {
        return {
            restrict: 'E',
            templateUrl: 'views/classica.html',
            controller: 'ThemeClassicaCtrl',
            controllerAs: 'vm'
        };
    });
}());

;(function() {
"use strict";

angular.module('upsites')
    .directive('themeNuevo', function() {
        return {
            restrict: 'E',
            templateUrl: 'views/nuevo.html',
            controller: 'ThemeNuevoCtrl',
            controllerAs: 'vm'
        };
    });
}());

;(function() {
"use strict";

angular.module('upsites')
    .directive('themeReorder', function() {
        return {
            restrict: 'E',
            templateUrl: 'views/themeReorder.html',
            controller: 'ThemeReorderCtrl',
            controllerAs: 'themeReorderVm'
        };
    });
}());

;(function() {
"use strict";

angular.module('upsites')
    .directive('themeSection', ["$compile", function($compile) {
        return {
            restrict: 'E',
            template: '<ng-include src="getTemplateUrl()"></ng-include>',
            link: function(scope, element, attrs) {
                scope.getTemplateUrl = function () {
                    return 'views/' + attrs.theme + '/' + attrs.sectionid + '.html';
                }
            }
        };
    }]);
}());

;(function() {
"use strict";

angular.module('upsites')
    .directive('themeSelector', function() {
        return {
            restrict: 'E',
            templateUrl: 'views/themeSelector.html',
            controller: 'ThemeSelectorCtrl',
            controllerAs: 'themeSelectorVm'
        };
    });
}());

;(function() {
"use strict";

angular.module('upsites')
    .directive('themeStream', function() {
        return {
            restrict: 'E',
            templateUrl: 'views/stream.html',
            controller: 'ThemeStreamCtrl',
            controllerAs: 'vm'
        };
    });
}());

;(function() {
"use strict";

angular.module('upsites')
    .directive('upsitesTheme', ["$compile", function($compile) {
        return {
            restrict: 'E',
            link: function(scope, element, attrs) {
                attrs.$observe('theme', function (theme) {
                    generateThemeDirective(theme);
                });
                function generateThemeDirective(theme) {
                    var generatedTemplate = '<theme-' + theme + '></theme-' + theme + '>';
                    element.empty();
                    element.append($compile(generatedTemplate)(scope));
                }
            }
        };
    }]);
}());

;(function() {
"use strict";

angular.module('upsites')
    .directive('nicescroll', function() {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                angular.element(element).niceScroll(scope.$eval(attrs.directiveName));
            }
        };
    });
}());

;(function() {
"use strict";

angular.module('upsites')
    .directive('parallax', function() {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                angular.element(element).parallax(scope.$eval(attrs.directiveName));
            }
        };
    });
}());

;(function() {
"use strict";

angular.module('upsites')
    .directive('rslider', ["$timeout", function($timeout) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                angular.element(element).royalSlider({
                    autoheight: true,
                    globalCaption: true,
                    keyboardNavEnabled: true,
                    autoHeight: true
                });
            }
        };
    }]);
}());
