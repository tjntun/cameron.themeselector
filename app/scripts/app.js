angular.module('upsites', [
    'angular.css.injector',
    'smoothScroll',
    'ui.sortable'
]).config(function($sceDelegateProvider) {
    $sceDelegateProvider.resourceUrlWhitelist([
        // Allow same origin resource loads.
        'self',
        // Allow loading from our assets domain.  Notice the difference between * and **.
        'https://www.youtube.com/**'
    ]);
});
