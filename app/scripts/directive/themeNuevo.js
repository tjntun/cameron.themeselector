angular.module('upsites')
    .directive('themeNuevo', function() {
        return {
            restrict: 'E',
            templateUrl: 'views/nuevo.html',
            controller: 'ThemeNuevoCtrl',
            controllerAs: 'vm'
        };
    });
