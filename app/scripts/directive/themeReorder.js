angular.module('upsites')
    .directive('themeReorder', function() {
        return {
            restrict: 'E',
            templateUrl: 'views/themeReorder.html',
            controller: 'ThemeReorderCtrl',
            controllerAs: 'themeReorderVm'
        };
    });
