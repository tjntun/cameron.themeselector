angular.module('upsites')
    .directive('themeStream', function() {
        return {
            restrict: 'E',
            templateUrl: 'views/stream.html',
            controller: 'ThemeStreamCtrl',
            controllerAs: 'vm'
        };
    });
