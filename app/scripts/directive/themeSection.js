angular.module('upsites')
    .directive('themeSection', function($compile) {
        return {
            restrict: 'E',
            template: '<ng-include src="getTemplateUrl()"></ng-include>',
            link: function(scope, element, attrs) {
                scope.getTemplateUrl = function () {
                    return 'views/' + attrs.theme + '/' + attrs.sectionid + '.html';
                }
            }
        };
    });
