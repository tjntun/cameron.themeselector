angular.module('upsites')
    .directive('themeClassica', function() {
        return {
            restrict: 'E',
            templateUrl: 'views/classica.html',
            controller: 'ThemeClassicaCtrl',
            controllerAs: 'vm'
        };
    });
