angular.module('upsites')
    .directive('editTools', function($compile) {
        return {
            restrict: 'E',
            templateUrl: 'views/editTools.html',
            controller: 'EditToolsCtrl',
            controllerAs: 'editToolsVm'
        };
    });
