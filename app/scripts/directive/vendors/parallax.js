angular.module('upsites')
    .directive('parallax', function() {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                angular.element(element).parallax(scope.$eval(attrs.directiveName));
            }
        };
    });
