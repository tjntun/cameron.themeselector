angular.module('upsites')
    .directive('nicescroll', function() {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                angular.element(element).niceScroll(scope.$eval(attrs.directiveName));
            }
        };
    });
