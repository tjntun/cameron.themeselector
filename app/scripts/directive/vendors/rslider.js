angular.module('upsites')
    .directive('rslider', function($timeout) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                angular.element(element).royalSlider({
                    autoheight: true,
                    globalCaption: true,
                    keyboardNavEnabled: true,
                    autoHeight: true
                });
            }
        };
    });
