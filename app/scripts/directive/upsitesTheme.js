angular.module('upsites')
    .directive('upsitesTheme', function($compile) {
        return {
            restrict: 'E',
            link: function(scope, element, attrs) {
                attrs.$observe('theme', function (theme) {
                    generateThemeDirective(theme);
                });
                function generateThemeDirective(theme) {
                    var generatedTemplate = '<theme-' + theme + '></theme-' + theme + '>';
                    element.empty();
                    element.append($compile(generatedTemplate)(scope));
                }
            }
        };
    });
