angular.module('upsites')
    .directive('themeSelector', function() {
        return {
            restrict: 'E',
            templateUrl: 'views/themeSelector.html',
            controller: 'ThemeSelectorCtrl',
            controllerAs: 'themeSelectorVm'
        };
    });
