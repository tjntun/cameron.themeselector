angular.module('upsites')
    .controller('ThemeNuevoCtrl', ThemeNuevoCtrl);

function ThemeNuevoCtrl($scope, $rootScope, $timeout) {
    var vm = this;
    // Destroy Stream theme mobile nav
    _j('body > #mobile-nav').remove();
    
    // $rootScope.$on('evt.themeColorChanged', function(e, data) {
    //     console.log(data);
    //     initMap(data.themeColor);
    // });

    // Google map
    var map;

    function initMap(themeNumber, mapId) {
        if (themeNumber == undefined || themeNumber == null) themeNumber = 0;
        if (mapId == undefined || mapId == null) mapId = "map";
        var place = { lat: 54.515102, lng: -128.610764 };
        map = new google.maps.Map(document.getElementById(mapId), {
            center: place,
            zoom: 13,
            styles: [{ "featureType": "water", "elementType": "geometry", "stylers": [{ "color": "#e9e9e9" }, { "lightness": 17 }] }, { "featureType": "landscape", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 20 }] }, { "featureType": "road.highway", "elementType": "geometry.fill", "stylers": [{ "color": "#ffffff" }, { "lightness": 17 }] }, { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{ "color": "#ffffff" }, { "lightness": 29 }, { "weight": 0.2 }] }, { "featureType": "road.arterial", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 18 }] }, { "featureType": "road.local", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 16 }] }, { "featureType": "poi", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 21 }] }, { "featureType": "poi.park", "elementType": "geometry", "stylers": [{ "color": "#dedede" }, { "lightness": 21 }] }, { "elementType": "labels.text.stroke", "stylers": [{ "visibility": "on" }, { "color": "#ffffff" }, { "lightness": 16 }] }, { "elementType": "labels.text.fill", "stylers": [{ "saturation": 36 }, { "color": "#333333" }, { "lightness": 40 }] }, { "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "transit", "elementType": "geometry", "stylers": [{ "color": "#f2f2f2" }, { "lightness": 19 }] }, { "featureType": "administrative", "elementType": "geometry.fill", "stylers": [{ "color": "#fefefe" }, { "lightness": 20 }] }, { "featureType": "administrative", "elementType": "geometry.stroke", "stylers": [{ "color": "#fefefe" }, { "lightness": 17 }, { "weight": 1.2 }] }]
        });
        var icon = 'assets/nuevo/img/icon/marker' + themeNumber + '.svg?t=' + Date.now();
        var marker = new google.maps.Marker({
            position: place,
            map: map,
            icon: icon
        });

        $timeout(function() {
            google.maps.event.trigger(map, 'resize');
            map.setCenter(place);
        }, 1000);
    }

    $timeout(function() {
        // Home section height
        if (isMobile.any) {
            angular.element('#home').css('height', _j(window).height() + 'px');
        }
        // All section height
        if (!isMobile.any) {
            angular.element('theme-section section').css('height', _j(window).height() + 'px');
            _j(window).resize(function(event) {
                _j('#wrapper > section').not('#contact').height(_j(window).height());
            });
        }
        
        // Nicescroll
        _j("html").niceScroll();
        if (!isMobile.any) _j(".niceScroll").niceScroll();

        // Mobile menu
        _j('#humburger').click(function(event) {
            event.stopPropagation();
            _j('#mobile-nav').slideDown('fast');
        });
        if (_j(window).width() < 1200) {
            _j('#mobile-nav .close').click(function() {
                _j('#mobile-nav').slideUp('fast');
            });
            _j('#mobile-nav').click(function(event) {
                event.stopPropagation();
            });
        }

        // Sticky nav
        _j(window).on('scroll', function(event) {
            stickyNav();
            updateActiveNav();
        });
        stickyNav();

        function stickyNav() {
            // if (_j(window).width() > 1200) {
            var scrollTop = _j(window).scrollTop();
            if (scrollTop >= _j('#features').offset().top) {
                _j('#secondary-nav').addClass('sticky');
            } else {
                _j('#secondary-nav').removeClass('sticky');
            }
            // }
        }

        updateActiveNav();



        // Feature section auto scroll
        if (_j(window).height() <= 900) {
            _j('#features .content').removeClass('flex-center-y').addClass('scrollable');
        }

        // Active nav
        var currentPageId = 'home';

        function updateActiveNav() {
            var scrollTop = _j(window).scrollTop();
            _j('#wrapper section').each(function(index, el) {
                var id = _j(this).attr('id');
                var adjust = 10;
                // Change current page title on #secondary-nav (aka sticky nav)
                if (scrollTop >= (_j(this).offset().top - adjust) && scrollTop < (_j(this).offset().top + _j(this).outerHeight())) {
                    var currentPageTitle = _j('#' + id + ' .section-title').text();
                    currentPageId = id;
                    if (id == 'gallery') currentPageTitle = 'Gallery';
                    if (id == 'auction') currentPageTitle = 'Inspections';
                    if (currentPageTitle != _j('#currentPageTitle .inner').text()) {
                        _j("#currentPageTitle .inner").fadeOut(function() {
                            _j(this).text(currentPageTitle).fadeIn();
                        });
                    }

                }
            });
        }

        // Next/prev section
        _j('#secondary-nav .icon-arrowdown').click(function() {
            // next
            var nextPageId = _j('#' + currentPageId).next().attr('id');
            scrollTo(_j('#' + nextPageId));
        });
        _j('#secondary-nav .icon-arrowup').click(function() {
            // back
            var prevPageId = _j('#' + currentPageId).prev().attr('id');
            scrollTo(_j('#' + prevPageId));
        });

        // Smooth scroll
        _j('a[href*="#"]:not([href="#"])').click(function() {
            stickyNav();
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = _j(this.hash);
                return scrollTo(target);
            }
        });

        function scrollTo(target) {
            target = target.length ? target : _j('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                _j('html, body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
            return false;
        }

        // Square gallery
        try {
            new SquareGallery({
                container: 'gallery',
                // qtyColumns: 3
            });
        } catch (e) {
            console.log(e)
        }
        initMap(0);
    }, 200);

}
