angular.module('upsites')
    .controller('ThemeSelectorCtrl', ThemeSelectorCtrl);

function ThemeSelectorCtrl($rootScope) {
    var themeSelectorVm = this;

    themeSelectorVm.hideThemeSelector = function() {
        angular.element('#themeSelector').addClass('hided');
    }

    themeSelectorVm.showThemeSelector = function() {
        angular.element('#themeSelector').removeClass('hided');
    }

    $rootScope.$on('evt.toggleThemeSelector', function(event, status) {
        if (status == 'show') {
            themeSelectorVm.showThemeSelector();
        } else {
            themeSelectorVm.hideThemeSelector();
        }
    });
}
