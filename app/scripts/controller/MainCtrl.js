angular.module('upsites')
    .controller('MainCtrl', MainCtrl);

function MainCtrl($scope, $rootScope, $timeout, cssInjector) {
    $rootScope.themeChangeableSections = [{
        id: 'features',
        title: 'Details',
    }, {
        id: 'gallery',
        title: 'Gallery',
    }, {
        id: 'location',
        title: 'Location',
    }, {
        id: 'auction',
        title: 'Inspections & Selling',
    }, {
        id: 'agent',
        title: 'Selling Agent',
    }, ];
    var mainVm = this;
    mainVm.themes = ['classica', 'nuevo', 'stream'];

    mainVm.currentTheme = mainVm.themes[0];
    applyTheme(mainVm.currentTheme);

    $scope.$watch('mainVm.currentTheme', function(theme, oldTheme) {
        if (theme != oldTheme) {
            applyTheme(theme);
            $timeout(function() {
                $rootScope.$emit('evt.toggleThemeSelector', 'hide');
            }, 1000);
        }
    });

    function applyTheme(theme) {
        cssInjector.removeAll();
        cssInjector.add("assets/" + theme + "/css/main.css");
        cssInjector.add("assets/" + theme + "/css/responsive.css");
        cssInjector.add("assets/" + theme + "/css/theme.css");
        cssInjector.add("assets/" + theme + "/fonts/web/fonts.css");

        if (theme == 'classica') {
            cssInjector.add("assets/" + theme + "/fonts/classica/style.css");
        }
        if (theme == 'nuevo') {
            cssInjector.add("assets/" + theme + "/fonts/iconfonts/icon-fonts.css");
        }
        if (theme == 'stream') {
            cssInjector.add("assets/" + theme + "/fonts/icomoon/icons.css");
            cssInjector.add("assets/" + theme + "/vendor/mmenu/css/jquery.mmenu.all.css");
        }
    }
}
