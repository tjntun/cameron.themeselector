angular.module('upsites')
    .controller('EditToolsCtrl', EditToolsCtrl);

function EditToolsCtrl($scope, $rootScope, $timeout, cssInjector) {
    var editToolsVm = this;

    editToolsVm.openThemeSelector = function() {
        angular.element('#themeSelector').removeClass('hided');
    }
    var nicescrollInited = false;
    editToolsVm.openThemeReorder = function() {
        if (!nicescrollInited) {
            _j('#themeReorder').niceScroll();
            nicescrollInited = true;
        }
        angular.element('#themeReorder').removeClass('hided');
    }

    $timeout(function() {
        // Theme picker
        var themeColor = localStorage.getItem('cameron_theme_color');
        if (themeColor == null) { themeColor = 0; }
        var themeBg = localStorage.getItem('cameron_theme_bg');
        if (themeBg == null) { themeBg = 1; }
        changeTheme(themeColor, themeBg);

        _j('#editTools .picker .group').click(function() {
            var themeColor = _j(this).attr('theme-color');
            var themeBg = _j(this).attr('theme-bg');
            changeTheme(themeColor, themeBg);

            localStorage.setItem('cameron_theme_color', themeColor);
            localStorage.setItem('cameron_theme_bg', themeBg);
        });

        function changeTheme(themeColor, themeBg) {
            $rootScope.$emit('changeTheme', themeColor);
            // Mark color pallete as selected
            _j('#editTools .group').removeClass('selected');
            _j('#editTools .group[theme-color="' + themeColor + '"][theme-bg="' + themeBg + '"]').addClass('selected');

            _j('body').removeClass('theme-color-0 theme-color-1 theme-color-2 theme-color-3 theme-color-4').addClass('theme-color-' + themeColor);
            _j('body').removeClass('theme-bg-1 theme-bg-2').addClass('theme-bg-' + themeBg);

            $rootScope.$emit('evt.themeColorChanged', { themeColor: themeColor, themeBg: themeBg });
        }

        // Load default font
        var fontBig = localStorage.getItem('cameron_font_big');
        if (fontBig != null) {
            _j('.font-big').css('font-family', fontBig);
            _j('#font-big-select [value=' + fontBig + ']').attr('selected', true);
        } else {
            fontBig = _j('#font-big-select').val();
            _j('.font-big').css('font-family', fontBig);
        }

        var fontSmall = localStorage.getItem('cameron_font_small');
        if (fontSmall != null) {
            _j('.font-small').css('font-family', fontSmall);
            _j('#font-small-select [value=' + fontSmall + ']').attr('selected', true);
        } else {
            fontSmall = _j('#font-small-select').val();
            _j('.font-small').css('font-family', fontSmall);
        }

        // Font picker
        _j('#font-big-select').change(function(event) {
            var font = _j(this).val();
            _j('.font-big').css('font-family', font);
            localStorage.setItem('cameron_font_big', font);
        });
        _j('#font-small-select').change(function(event) {
            var font = _j(this).val();
            _j('.font-small, .content ul, p').css('font-family', font);
            localStorage.setItem('cameron_font_small', font);
        });
    });
}
