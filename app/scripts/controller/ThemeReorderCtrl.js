angular.module('upsites')
    .controller('ThemeReorderCtrl', ThemeReorderCtrl);

function ThemeReorderCtrl($rootScope, $scope) {
    var themeReorderVm = this,
        nicescrollInited = false;
    themeReorderVm.themeChangeableSections = $rootScope.themeChangeableSections;

    themeReorderVm.sortableOptions = {
        appendTo: '#reorderSections',
        update: function () {
            $rootScope.$emit('evt.orderChanged');
        }
    }

    ////////

    themeReorderVm.hideThemeReorder = function() {
        angular.element('#themeReorder').addClass('hided');
    }

    themeReorderVm.showThemeReorder = function() {
        
        angular.element('#themeReorder').removeClass('hided');
    }

    $rootScope.$on('evt.toggleThemeReorder', function(event, status) {
        if (status == 'show') {
            themeReorderVm.showThemeReorder();
        } else {
            themeReorderVm.hideThemeReorder();
        }
    });
}
