angular.module('upsites')
    .controller('ThemeClassicaCtrl', ThemeClassicaCtrl);

function ThemeClassicaCtrl($scope, $rootScope, $timeout, $window, $sce) {
    var vm = this;
    vm.featuresReadmore = false;
    vm.agents = [{
        id: 1,
        avatar: 'assets/classica/img/samantha.jpg',
        firstname: 'SAMANTHA',
        lastname: 'PAULINSON',
        quote: 'It makes me proud to be part of a team with such high integrity and commitment to serving our clients',
        about: 'A trusted advisor with an excellent sales record, John O’Reilly is an influential member to the McGrath team in the Thirroul Office. Specialising in the suburbs of Woonona, Bulli, Thirroul and Austinmer, Johns’ passion for his work is almost equal to that for his two daughters. He brings a high energy approach to his work and has unparalleled drive to continually raise the benchmark. While outcome driven, Johns’ caring attitude stems from his motivation to match the right buyers with their dream home. A commitment to ongoing industry training and professional development sets John apart from the average real estate agent, making him a perfect fit with the McGrath network. Johns’ professional ambition is closely aligned with McGrath’s ethos of continuously improving staff skills to ensure a ‘six star’ quality of service. For residential sales in Thirroul and surrounding areas, contact John O’Reilly to find out how his skills and experience can benefit your sale.',
        video: 'https://www.youtube.com/embed/Jer8XjMrUB4'
    }, {
        id: 2,
        avatar: 'assets/classica/img/robert.jpg',
        firstname: 'ROBERT',
        lastname: 'MICHAELS',
        quote: 'It makes me proud to be part of a team',
        about: 'ROBERT passion for his work is almost equal to that for his two daughters. He brings a high energy approach to his work and has unparalleled drive to continually raise the benchmark. While outcome driven, Johns’ caring attitude stems from his motivation to match the right buyers with their dream home. A commitment to ongoing industry training and professional development sets John apart from the average real estate agent, making him a perfect fit with the McGrath network. Johns’ professional ambition is closely aligned with McGrath’s ethos of continuously improving staff skills to ensure a ‘six star’ quality of service. For residential sales in Thirroul and surrounding areas, contact John O’Reilly to find out how his skills and experience can benefit your sale.',
        video: 'https://www.youtube.com/embed/4KSlB-eA3u4'
    }];
    vm.selectedAgent = vm.agents[0];
    $sce.trustAsResourceUrl(vm.selectedAgent.video);

    vm.closeMenu = closeMenu;
    vm.openMenu = openMenu;
    vm.openSidePanel = openSidePanel;
    vm.closeSidePanel = closeSidePanel;

    ///////////////

    function openSidePanel(agent) {
        vm.selectedAgent = agent;
        $sce.trustAsResourceUrl(vm.selectedAgent.video);
        vm.slideIn = true;
        _j(".niceScroll").niceScroll();
    }

    function closeSidePanel() {
        vm.slideIn = false;
        _j(".niceScroll").getNiceScroll().remove();
    }

    function openMenu() {
        _j('#mobile-nav').slideDown('fast');
    }

    function closeMenu() {
        _j('#mobile-nav').slideUp('fast');
    }

    // Show/hide video iframe
    _j('.video-modal').on('show.bs.modal', function() {
        var src = _j(this).find('iframe').data('src');
        _j(this).find('iframe').attr('src', src);
    });
    _j('.video-modal').on('hide.bs.modal', function() {
        _j(this).find('iframe').attr('src', '');
    });

    // Destroy Stream theme mobile nav
    _j('body > #mobile-nav').remove();
    _j('body > .parallax-mirror').remove();

    // Map
    var map;

    function initMap(themeNumber, mapId) {
        if (themeNumber == undefined || themeNumber == null) themeNumber = 0;
        if (mapId == undefined || mapId == null) mapId = "map";
        var place = { lat: 54.515102, lng: -128.610764 };
        map = new google.maps.Map(document.getElementById(mapId), {
            center: place,
            zoom: 13
        });
        google.maps.event.trigger(map, 'resize');
        var icon = 'assets/classica/img/icon/theme-color-' + themeNumber + '/marker.png?t=' + Date.now();
        var marker = new google.maps.Marker({
            position: place,
            map: map,
            icon: icon
        });
        $timeout(function() {
            google.maps.event.trigger(map, 'resize');
            map.setCenter(place);
        }, 1000);
    }

    // Listen for theme change event
    $rootScope.$on('changeTheme', function(event, themeColor) {
        initMap(themeColor);
    });

    $timeout(function function_name() {
        // _j(".niceScroll").niceScroll();

        $rootScope.$on('evt.orderChanged', function() {
            smoothScroll();
            updateActiveNav();
        });

        if (isMobile.any) {
            angular.element('#home').height(angular.element(window).height());
        }
        var themeColor = localStorage.getItem('cameron_theme_color');
        if (themeColor) {
            initMap(themeColor);
        } else {
            initMap(0);
        }

        smoothScroll();

        function smoothScroll() {
            // Smooth scroll
            _j('a[href*="#"]:not([href="#"])').click(function() {
                stickyNav();
                if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                    var target = _j(this.hash);
                    target = target.length ? target : _j('[name=' + this.hash.slice(1) + ']');
                    if (target.length) {
                        _j('html, body').animate({
                            scrollTop: target.offset().top
                        }, 1000);
                        return false;
                    }
                }
            });
        }

        // Sticky nav
        angular.element($window).bind('scroll', function(event) {
            stickyNav();
            updateActiveNav();
        });

        function stickyNav() {
            // if ($window.innerWidth > 1200) {
            var scrollTop = $window.pageYOffset;
            if (scrollTop >= _j('#home').height()) {
                angular.element('#secondary-nav').addClass('sticky');
            } else {
                angular.element('#secondary-nav').removeClass('sticky');
            }
            // }
        }

        updateActiveNav();

        // Active nav
        function updateActiveNav() {
            var scrollTop = $window.pageYOffset;
            _j('#wrapper section').each(function(index, el) {
                var id = _j(this).attr('id');
                var adjust = 10;
                if (id == 'contact') adjust = 100;
                if (scrollTop >= (_j(this).offset().top - adjust) && scrollTop <= (_j(this).offset().top + _j(this).outerHeight())) {
                    _j('nav a').removeClass('active');
                    _j('nav a[href="#' + id + '"]').not('.logo').addClass('active');
                }
            });
        }
        // Mobile menu
        // _j('#humburger').click(function(event) {
        //     event.stopPropagation();
        //     _j('#mobile-nav').slideDown('fast');
        // });
        if (_j(window).width() < 1200) {
            // _j('#mobile-nav .close').click(function() {
            //     _j('#mobile-nav').slideUp('fast');
            // });
            _j('#mobile-nav').click(function(event) {
                event.stopPropagation();
            });
        }

        // Royal slider
        var si = _j('#gallery-1').royalSlider({
            addActiveClass: true,
            arrowsNav: false,
            controlNavigation: 'none',
            loop: true,
            fadeinLoadedSlide: false,
            globalCaption: true,
            navigateByClick: false,
            keyboardNavEnabled: true,
            globalCaptionInside: false,
            video: {
                autoHideArrows: true,
                autoHideControlNav: false,
                autoHideBlocks: true,
                youTubeCode: '<iframe src="https://www.youtube.com/embed/%id%?rel=1&autoplay=1&showinfo=0" frameborder="no" allowFullscreen></iframe>'
            },
        }).data('royalSlider');

        _j('#gallery-nav .current').text(si.currSlideId + 1);
        _j('#gallery-nav .total').text(si.numSlides);

        _j('#gallery-nav .next').click(function(event) {
            si.next();
            _j('#gallery-nav .current').text(si.currSlideId + 1);
        });
        _j('#gallery-nav .prev').click(function(event) {
            si.prev();
            _j('#gallery-nav .current').text(si.currSlideId + 1);
        });
        si.ev.on('rsAfterSlideChange', function(event) {
            _j('#gallery-nav .current').text(si.currSlideId + 1);
            if (si.currSlideId != 0) {
                _j('#gallery .section-title, #gallery .hr').addClass('fade');
            } else {
                _j('#gallery .section-title, #gallery .hr').removeClass('fade');
            }
        });

        // Location
        _j('#mapModal').on('shown.bs.modal', function() {
            initMap(null, 'map2');
        });

        _j('#lifestyle .launch').magnificPopup({
            items: [{
                src: 'assets/classica/img/gallery-preview.jpg'
            }, {
                src: 'https://vimeo.com/185383679',
                type: 'iframe' // this overrides default type
            }],
            gallery: {
                enabled: true
            },
            type: 'image' // this is default type
        });
    }, 800);
}
