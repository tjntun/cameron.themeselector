angular.module('upsites')
    .controller('ThemeStreamCtrl', ThemeStreamCtrl);

function ThemeStreamCtrl($timeout, $rootScope, $scope) {
    var vm = this;

    vm.nextSlide = function(slideId) {
        if (!slideId) slideId = 'carousel';
        _j('#' + slideId).carousel('next');
    }
    vm.prevSlide = function(slideId) {
        if (!slideId) slideId = 'carousel';
        _j('#' + slideId).carousel('prev');
    }

    vm.toggleMobileMenu = function() {
        _j('#mobile-nav').slideToggle('fast', 'swing');
    }

    // Google map
    var map;

    function initMap(themeNumber, mapId) {
        try {
            if (themeNumber == undefined || themeNumber == null) themeNumber = 0;
            if (mapId == undefined || mapId == null) mapId = "map";
            var place = { lat: 54.515102, lng: -128.610764 };
            map = new google.maps.Map(document.getElementById(mapId), {
                center: place,
                zoom: 13,
                scrollwheel: false,
                styles: [{ "featureType": "water", "elementType": "geometry", "stylers": [{ "color": "#e9e9e9" }, { "lightness": 17 }] }, { "featureType": "landscape", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 20 }] }, { "featureType": "road.highway", "elementType": "geometry.fill", "stylers": [{ "color": "#ffffff" }, { "lightness": 17 }] }, { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{ "color": "#ffffff" }, { "lightness": 29 }, { "weight": 0.2 }] }, { "featureType": "road.arterial", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 18 }] }, { "featureType": "road.local", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 16 }] }, { "featureType": "poi", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 21 }] }, { "featureType": "poi.park", "elementType": "geometry", "stylers": [{ "color": "#dedede" }, { "lightness": 21 }] }, { "elementType": "labels.text.stroke", "stylers": [{ "visibility": "on" }, { "color": "#ffffff" }, { "lightness": 16 }] }, { "elementType": "labels.text.fill", "stylers": [{ "saturation": 36 }, { "color": "#333333" }, { "lightness": 40 }] }, { "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "transit", "elementType": "geometry", "stylers": [{ "color": "#f2f2f2" }, { "lightness": 19 }] }, { "featureType": "administrative", "elementType": "geometry.fill", "stylers": [{ "color": "#fefefe" }, { "lightness": 20 }] }, { "featureType": "administrative", "elementType": "geometry.stroke", "stylers": [{ "color": "#fefefe" }, { "lightness": 17 }, { "weight": 1.2 }] }]
            });
            var icon = 'assets/stream/img/icon/marker' + themeNumber + '.svg?t=' + Date.now();
            var marker = new google.maps.Marker({
                position: place,
                map: map,
                icon: icon
            });

            $timeout(function() {
                google.maps.event.trigger(map, 'resize');
                map.setCenter(place);
            }, 1000);
        } catch (e) {
            console.log(e);
        }
    }

    $timeout(function() {
        var gallerySlider = _j('#carousel .royalSlider').data('royalSlider');

        vm.gallerySliderNext = function() {
            gallerySlider.next();
        }
        vm.gallerySliderPrev = function() {
            gallerySlider.prev();
        }
        var videoSlider = _j('#video-carousel .royalSlider').data('royalSlider');

        vm.videoSliderNext = function() {
            videoSlider.next();
        }
        vm.videoSliderPrev = function() {
            videoSlider.prev();
        }

        // Show/hide video iframe
        _j('.video-modal').on('show.bs.modal', function() {
            var src = _j(this).find('iframe').data('src');
            _j(this).find('iframe').attr('src', src);
        });
        _j('.video-modal').on('hide.bs.modal', function() {
            _j(this).find('iframe').attr('src', '');
        });

        initMap(0);
        // Nicescroll

        if (!isMobile.any) {
            _j("html").niceScroll();
        }

        // Home section height
        if (isMobile.any) {
            _j('#home').height(window.innerHeight);
            angular.element('.nicescroll-rails').remove();
        }

        // Re-position features item
        if (_j(window).width() < 568) {
            vm.featureMustReArrange = true;
        } else {
            vm.featureMustReArrange = false;
        }
        _j(window).on('resize', function(event) {
            if (_j(window).width() < 568) {
                vm.featureMustReArrange = true;
            } else {
                vm.featureMustReArrange = false;
            }
        });

        // Sticky nav
        _j(window).on('scroll', function(event) {
            stickyNav();
            updateActiveNav();
        });
        stickyNav();

        function stickyNav() {
            try {
                var scrollTop = _j(window).scrollTop();
                if (scrollTop > 0 && scrollTop >= _j('#home').height()) {
                    _j('#secondary-nav').addClass('sticky');
                } else {
                    _j('#secondary-nav').removeClass('sticky');
                }
            } catch (e) {
                console.log(e);
            }
        }

        updateActiveNav();

        // All section height

        if (!isMobile.any) {
            _j('#wrapper > section').not('#contact').css('min-height', _j(window).height() + 'px');
            _j(window).resize(function(event) {
                _j('#wrapper > section').not('#contact').css('min-height', _j(window).height() + 'px');
            });
        } else {
            _j('#gallery').css('min-height', _j(window).height() + 'px');
        }

        // Active nav
        var currentPageId = 'home';

        function updateActiveNav() {
            var scrollTop = _j(window).scrollTop();
            _j('#wrapper section').each(function(index, el) {
                var id = _j(this).attr('id');
                var adjust = 85;
                if (scrollTop >= (_j(this).offset().top - adjust) && scrollTop <= (_j(this).offset().top + _j(this).outerHeight())) {
                    _j('nav a').removeClass('active');
                    _j('nav a[href="#' + id + '"]').not('.logo').addClass('active');
                }
            });
        }

        // Next/prev section
        _j('#secondary-nav .icon-arrowdown').click(function() {
            // next
            var nextPageId = _j('#' + currentPageId).next().attr('id');
            scrollTo(_j('#' + nextPageId));
        });
        _j('#secondary-nav .icon-arrowup').click(function() {
            // back
            var prevPageId = _j('#' + currentPageId).prev().attr('id');
            scrollTo(_j('#' + prevPageId));
        });

        // Smooth scroll
        _j('a[href*="#"]:not([href="#"])').click(function() {
            stickyNav();
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = _j(this.hash);
                if (_j(this).parents('nav').attr('id') == 'mobile-nav') vm.toggleMobileMenu();
                return scrollTo(target);
            }
        });

        function scrollTo(target) {
            target = target.length ? target : _j('[name=' + this.hash.slice(1) + ']');
            var adjust = 80;
            if (target.attr('id') == 'features') adjust = 0;
            if (target.length) {
                _j('html, body').animate({
                    scrollTop: target.offset().top - adjust
                }, 1000);
                return false;
            }
            return false;
        }

    }, 500);


}
