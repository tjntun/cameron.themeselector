// Include gulp
var gulp = require('gulp');

// Include Our Plugins
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var ngAnnotate = require('gulp-ng-annotate');
var iife = require("gulp-iife");
var streamqueue = require('streamqueue');

// Concat & Minify App JS
gulp.task('scripts', function() {
    return gulp.src('app/scripts/**/*.js')
        .pipe(iife())
        .pipe(ngAnnotate())
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest('app/dist'))
        .pipe(rename('scripts.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('app/dist'));
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch('app/scripts/**/*', ['scripts']);
});

// Default Task
gulp.task('default', ['scripts', 'watch']);
